tabuada :: Int -> IO ()
tabuada n = mapM_ putStrLn [ show y ++ " x " ++ show x ++ " = " ++ show (x*y) | x <- [1..10], let y = n ]

tabuadaX :: Int -> Int -> IO ()
tabuadaX a b = mapM_ putStrLn [ show y ++ " x " ++ show x ++ " = " ++ show (x*y) | x <- [1..b], let y = a ]