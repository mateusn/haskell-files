data RPS = Rock | Paper | Scissors
data Player = PlayerOne | PlayerTwo | Draw

instance Show Player where
    show PlayerOne = "Player 1"
    show PlayerTwo = "Player 2"
    show Draw = "Draw"

beats :: RPS -> RPS -> Bool
Rock `beats` Scissors = True
Paper `beats` Rock = True
Scissors `beats` Paper = True
_ `beats` _ = False

getRPS :: String -> RPS
getRPS "Paper" = Paper
getRPS "Scissors" = Scissors
getRPS "Rock" = Rock

getWinner :: String -> String -> Player
getWinner p1 p2
    | beats (getRPS p1) (getRPS p2) = PlayerOne
    | beats (getRPS p2) (getRPS p1) = PlayerTwo
    | otherwise = Draw

main = do
    putStr "Player 1: "
    p1Input <- getLine
    putStr "Player 2: "
    p2Input <- getLine

    putStrLn $ show $ getWinner p1Input p2Input
